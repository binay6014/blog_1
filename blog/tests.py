from django.test import TestCase,Client
from django.contrib.auth import get_user_model
from django.urls import reverse
from .models import Post


class BlogTests(TestCase):

	def setUp(self):
		self.user = get_user_model().objects.create_user(
			username='test_user',
			email='test@gmail.com',
			password='1234'
		)

		self.post = Post.objects.create(
			title = "A good title",
			body = "Body of the post",
			author = self.user
		)

	def test_string(self):
		post = Post(title="Simple title")
		self.assertEqual(str(post),post.title)

	def test_post_content(self):
		self.assertEqual(f'{self.post.title}',"A good title")
		self.assertEqual(f'{self.post.body}',"Body of the post")
		self.assertEqual(f'{self.post.author}','test_user')

	def test_post_list_view(self):
		response = self.client.get(reverse('home'))
		self.assertEqual(response.status_code,200)
		self.assertContains(response,"A good title")
		self.assertTemplateUsed(response,'blog/home.html')

	def test_post_detail_view(self):
		resp = self.client.get('/post/1')
		no_resp = self.client.get('/post/1000')
		self.assertEqual(resp.status_code,200)
		self.assertEqual(no_resp.status_code,404)
		self.assertContains(resp,'A good title')
		self.assertTemplateUsed(resp,'blog/post_detail.html')

	def test_get_absolute_url(self):
		self.assertEqual(self.post.get_absolute_url(),'/post/1')
